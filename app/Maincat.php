<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Maincat extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    public function procats() {
        return $this->hasMany('App\Procat') ;
    }
}
