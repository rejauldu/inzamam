<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Procat extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'maincat_id'
    ];
    public function maincat() {
        return $this->belongsTo('App\Maincat');
    }
    public function subprocats() {
        return $this->hasMany('App\Subprocat') ;
    }
}
