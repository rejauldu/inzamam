<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subprocat extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    public function procat() {
        return $this->belongsTo('App\Procat');
    }
}
