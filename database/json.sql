-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 20, 2021 at 12:03 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `json`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `maincats`
--

CREATE TABLE `maincats` (
  `id` int(11) NOT NULL,
  `name` varchar(191) CHARACTER SET latin1 NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `maincats`
--

INSERT INTO `maincats` (`id`, `name`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Living', 1, '2021-06-08 05:59:22', '2021-06-08 05:59:22'),
(2, 'Bedroom', 1, '2021-06-08 06:34:33', '2021-06-08 06:34:33'),
(4, 'Dining', 1, '2021-06-12 14:53:39', '2021-06-12 14:53:39'),
(5, 'Storage', 1, '2021-06-12 14:55:17', '2021-06-12 14:55:17'),
(6, 'Office', 1, '2021-06-12 14:55:59', '2021-06-12 14:55:59'),
(7, 'Study', 1, '2021-06-12 14:59:46', '2021-06-12 14:59:46'),
(8, 'Kitchen', 1, '2021-06-12 15:02:40', '2021-06-12 15:02:40'),
(9, 'Door', 1, '2021-06-12 15:02:51', '2021-06-12 15:02:51'),
(10, 'Outdoor', 1, '2021-06-12 15:03:57', '2021-06-12 15:03:57'),
(11, 'Interior', 1, '2021-06-12 15:04:07', '2021-06-12 15:04:07'),
(12, 'Sale', 1, '2021-06-12 15:04:25', '2021-06-12 15:04:25');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `procats`
--

CREATE TABLE `procats` (
  `id` int(10) UNSIGNED NOT NULL,
  `maincat_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `banner` varchar(191) DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `procats`
--

INSERT INTO `procats` (`id`, `maincat_id`, `name`, `banner`, `active`, `created_at`, `updated_at`) VALUES
(1, 1, 'Sofa', NULL, 1, '2021-06-08 09:17:45', '2021-06-08 09:17:45'),
(4, 1, 'Center Table', NULL, 1, '2021-06-08 09:46:53', '2021-06-08 09:46:53'),
(5, 2, 'Bed', NULL, 1, '2021-06-08 09:47:10', '2021-06-08 09:47:10'),
(6, 1, 'Sofa Set', NULL, 1, '2021-06-12 15:05:59', '2021-06-12 15:05:59'),
(7, 1, 'Chairs', NULL, 1, '2021-06-12 15:06:30', '2021-06-12 15:06:30'),
(8, 1, 'Tables', NULL, 1, '2021-06-12 15:07:02', '2021-06-12 15:07:02'),
(9, 1, 'Divan', NULL, 1, '2021-06-12 15:07:54', '2021-06-12 15:07:54'),
(10, 1, 'Shoe Rack', NULL, 1, '2021-06-12 15:08:05', '2021-06-12 15:08:05'),
(11, 1, 'Belcony', NULL, 1, '2021-06-12 15:08:54', '2021-06-12 15:08:54'),
(12, 1, 'TV Stands', NULL, 1, '2021-06-12 15:09:40', '2021-06-12 15:09:40'),
(13, 1, 'Recliners', NULL, 1, '2021-06-12 15:10:38', '2021-06-12 15:10:38'),
(14, 1, 'Shelves', NULL, 1, '2021-06-12 15:11:22', '2021-06-12 15:11:22'),
(15, 2, 'Beds', NULL, 1, '2021-06-12 15:11:44', '2021-06-12 15:11:44'),
(16, 2, 'Wardrobe', NULL, 1, '2021-06-12 15:30:21', '2021-06-12 15:30:21'),
(17, 2, 'Dressing Table', NULL, 1, '2021-06-12 15:30:46', '2021-06-12 15:30:46'),
(18, 2, 'Mattresses', NULL, 1, '2021-06-12 15:31:32', '2021-06-12 15:31:32'),
(19, 2, 'Chest Of Drawers', NULL, 1, '2021-06-12 15:32:03', '2021-06-12 15:32:03'),
(20, 2, 'Bed Side Table', NULL, 1, '2021-06-12 15:32:37', '2021-06-12 15:32:37'),
(21, 2, 'Almirah', NULL, 1, '2021-06-12 15:33:46', '2021-06-12 15:33:46'),
(22, 2, 'Pillows', NULL, 1, '2021-06-12 15:34:07', '2021-06-12 15:34:07'),
(23, 4, 'Dining Tables & Sets', NULL, 1, '2021-06-12 15:36:45', '2021-06-12 15:36:45'),
(24, 4, 'Dining Chairs', NULL, 1, '2021-06-12 15:38:29', '2021-06-12 15:38:29'),
(25, 4, 'Cafeteria Furniture', NULL, 1, '2021-06-12 15:40:37', '2021-06-12 15:40:37'),
(26, 4, 'Tea Table', NULL, 1, '2021-06-12 15:40:51', '2021-06-12 15:40:51'),
(27, 4, 'Showcase', NULL, 1, '2021-06-12 15:42:35', '2021-06-12 15:42:35'),
(28, 6, 'Desks & Tables', NULL, 1, '2021-06-12 15:43:04', '2021-06-12 15:43:04'),
(29, 6, 'Office Chairs', NULL, 1, '2021-06-12 15:44:57', '2021-06-12 15:44:57'),
(30, 6, 'Drawer Unit', NULL, 1, '2021-06-12 15:45:08', '2021-06-12 15:45:08'),
(31, 6, 'Cabinet', NULL, 1, '2021-06-12 15:46:03', '2021-06-12 15:46:03'),
(32, 6, 'Racks', NULL, 1, '2021-06-12 15:48:47', '2021-06-12 15:48:47'),
(33, 6, 'Office Almirah', NULL, 1, '2021-06-12 15:49:00', '2021-06-12 15:49:00'),
(34, 7, 'Study Table', NULL, 1, '2021-06-12 15:49:16', '2021-06-12 15:49:16'),
(35, 7, 'Study Chairs', NULL, 1, '2021-06-12 15:49:29', '2021-06-12 15:49:29'),
(36, 6, 'Book Shelves', NULL, 1, '2021-06-12 15:49:38', '2021-06-12 15:49:38'),
(37, 7, 'Lamp Stands', NULL, 1, '2021-06-12 15:50:18', '2021-06-12 15:50:18'),
(38, 9, 'Wooden Doors', NULL, 1, '2021-06-12 15:51:13', '2021-06-12 15:51:13'),
(39, 9, 'Glass Door', NULL, 1, '2021-06-12 15:51:42', '2021-06-12 15:51:42'),
(40, 10, 'Outdoor Table', NULL, 1, '2021-06-12 15:53:20', '2021-06-12 15:53:20'),
(41, 10, 'Cafe Chairs', NULL, 1, '2021-06-12 15:53:33', '2021-06-12 15:53:33');

-- --------------------------------------------------------

--
-- Table structure for table `subprocats`
--

CREATE TABLE `subprocats` (
  `id` int(10) UNSIGNED NOT NULL,
  `maincat_id` int(11) NOT NULL,
  `procat_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordering` int(11) DEFAULT NULL,
  `banner` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subprocats`
--

INSERT INTO `subprocats` (`id`, `maincat_id`, `procat_id`, `name`, `ordering`, `banner`, `active`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Wooden Sofa', NULL, NULL, 1, '2021-06-08 13:49:58', '2021-06-08 13:49:58'),
(2, 1, 1, 'One Seater', NULL, NULL, 1, '2021-06-13 14:51:06', '2021-06-13 14:51:06'),
(3, 1, 1, 'Two Seater', NULL, NULL, 1, '2021-06-13 14:51:20', '2021-06-13 14:51:20'),
(4, 1, 1, '3 Seater Sofa', NULL, NULL, 1, '2021-06-13 14:52:20', '2021-06-13 14:52:20'),
(5, 1, 1, 'L Shape Sofa', NULL, NULL, 1, '2021-06-13 14:52:35', '2021-06-13 14:52:35'),
(6, 2, 5, 'King Size Bed', NULL, NULL, 1, '2021-06-13 14:56:48', '2021-06-13 14:56:48'),
(7, 2, 5, 'Queen Size Bed', NULL, NULL, 1, '2021-06-13 14:57:00', '2021-06-13 14:57:00'),
(8, 2, 5, 'Double Bed', NULL, NULL, 1, '2021-06-13 14:57:29', '2021-06-13 14:57:29'),
(9, 2, 5, 'Single Bed', NULL, NULL, 1, '2021-06-13 14:57:57', '2021-06-13 14:57:57'),
(10, 2, 5, 'Semi-Double Bed', NULL, NULL, 1, '2021-06-13 14:58:18', '2021-06-13 14:58:18');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `maincats`
--
ALTER TABLE `maincats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `procats`
--
ALTER TABLE `procats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `miancat_id` (`maincat_id`);

--
-- Indexes for table `subprocats`
--
ALTER TABLE `subprocats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subprocats_procat_id_foreign` (`procat_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `maincats`
--
ALTER TABLE `maincats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `procats`
--
ALTER TABLE `procats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `subprocats`
--
ALTER TABLE `subprocats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
